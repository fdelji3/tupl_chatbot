from flask import Flask, render_template, request, session
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from yaml import emit
from stages import stages, names
import secrets
import re


NAME = None

# Flask initialization
app = Flask("Polite Car Service", template_folder='templates',
            static_folder='static')


# Generate new key for each connection
secret = secrets.token_urlsafe(32)
app.secret_key = "RandomString786023"


# Create pretrained chatbot
chatbot = ChatBot(
    'Car Service',
    logic_adapters=[
        'chatterbot.logic.BestMatch',
        {
            'import_path': 'chatterbot.logic.SpecificResponseAdapter',
            'input_text': 'appointment',
            'output_text': 'Ok, please insert the following data: username,date,car model'
        }
    ],
)

trainer = ListTrainer(chatbot)
# Train the chat bot with a few responses
trainer.train([
    'How can I help you?',
    "can you create an appointment for me?",
    'ok, Please enter your date of preference, full name and car model',
    'I want to create an appointment',
    'ok, Please enter your date of preference, full name and car model',
    '12/07/2022, Leostaquio,seat ibiza',
    'ok, appointment created on date 12/07/2022 for your seat ibiza, thank you francisco'
])


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/get", methods=["GET", "POST"])
def chatbot_response():
    # regexp to find dates
    global NAME
    msg = request.form["msg"]
    for i in names:
        if msg.__contains__(i):
             NAME = i
    if int(request.form["counter"]) > 1 and NAME == None:
        return "Hi!, Please enter your name so we can know each other"
    if msg.__contains__("appointment") and NAME != None:
        return "ok, Please enter your date of preference and your car model separeted by commas"
    x = re.search("^([1-9] |1[0-9]| 2[0-9]|3[0-1])(.|-)([1-9] |1[0-2])(.|-|)20[0-9][0-9]$", msg)
    if x:
        response = msg.split[msg]
        response = f"Ok we have created an appointment for you mr {NAME} {response[0]} for your {response[2]}"
    else:
        response = chatbot.get_response(msg)

    return str(response)


app.run(host='localhost', port=5000)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
