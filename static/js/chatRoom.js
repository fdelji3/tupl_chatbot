$(document).ready(function() {
    var counter = 0;
    $("form").on("submit", function(event) {
        var rawText = $("#text").val();
        var userHtml = '<p class="userText"><span>' + rawText + "</span></p>";
        $("#text").val("");
        $("#chatbox").append(userHtml);
        document.getElementById("userInput").scrollIntoView({
            block: "start",
            behavior: "smooth",
        });
        $.ajax({
            data: {
                msg: rawText,
                counter: counter
            },
            type: "POST",
            url: "/get",
        }).done(function(data) {
            var botHtml = '<p class="botText"><span>' + data + "</span></p>";
            counter = counter + 1;
            $("#chatbox").append($.parseHTML(botHtml));
            document.getElementById("userInput").scrollIntoView({
                block: "start",
                behavior: "smooth",
            });
        });
        event.preventDefault();
    });
});