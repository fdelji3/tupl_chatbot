FROM python:3

RUN apt-get update -y
RUN apt-get install -y python3-dev  build-essential python-pip gunicorn
RUN pip install --upgrade setuptools



COPY . /app
WORKDIR /app                                   
RUN pip install -r ./app/requirements.txt
 